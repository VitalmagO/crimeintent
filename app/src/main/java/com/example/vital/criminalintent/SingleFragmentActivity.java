/*Субклассы SingleFragmentActivity реализуют createFragment() так, чтобы он возвра-
щал экземпляр фрагмента, хостом которого является активность.*/

package com.example.vital.criminalintent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public abstract class SingleFragmentActivity extends AppCompatActivity{

    protected abstract Fragment createFragment();   //Создание экземпляра фрагмента.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentManager fm = getSupportFragmentManager();   //Из библиотеки поддержки v4. Получаем FragmentManager.

        /*Передаем фрагмент для управления*/
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);   //Получение экземпляра CrimeFragment от FragmentManager

        if (fragment == null) { //Проверка отсутствия фрагмента. Сделано так потому, что при уничтожении активности ее экземпляр FragmentManager сохраняет список фрагментов. И при воссоздании
            //активности, загружает список, чтобы все работало как прежде.
            fragment = createFragment(); //Создание и закрепление транзакции фрагмента
            fm.beginTransaction()   //Создать новую транзакцию фрагмента
                    .add(R.id.fragment_container, fragment) //Включить в нее одну операцию add. 1 - идентификатор контейнерного представления. Сообщает FragmentManager где в представлении
                    //активности должно находится представление фрагмента и обеспечивает однозначную идентификацию фрагмента в списке FragmentManager
                    .commit();  //Закрепить
        }
        /**/
    }
}
