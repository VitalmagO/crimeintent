package com.example.vital.criminalintent;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CrimeLab { //Синглетный класс (Singleton class)
    private static CrimeLab sCrimeLab;  //Префикс s означает static, используется как условное обозначение

    private List<Crime> mCrimes;

    public static CrimeLab get(Context context) {   //Закрытый конструктор. Другие классы не смогут создать экземпляр CrimeLab в обход
        // метода get()
        if (sCrimeLab == null) {
            sCrimeLab = new CrimeLab(context);
        }
        return sCrimeLab;
    }
    private CrimeLab(Context context) {
        mCrimes = new ArrayList<>();
        for (int i = 0;i < 100; i++) {
            Crime crime = new Crime();
            crime.setTitle("Crime #" + i);
            crime.setSolved(i % 2 == 0);    //Для каждого второго объекта
            mCrimes.add(crime);
        }
    }

    public List<Crime> getCrimes() {
        return mCrimes;
    }
    public Crime getCrime(UUID id) {
        for (Crime crime : mCrimes) {
            if (crime.getId().equals(id)) {
                return crime;
            }
        }
        return null;
    }
}
