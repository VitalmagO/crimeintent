package com.example.vital.criminalintent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import static android.widget.CompoundButton.*;

public class CrimeFragment extends Fragment {
    private Crime mCrime;
    private EditText mTitleField;
    private Button mDateButton;
    private CheckBox mSolvedCheckBox;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) { //Открытый метод, в отличии от закрытых в активити. Здесь настраивается экземпляр фрагмента.
        super.onCreate(savedInstanceState);
        mCrime = new Crime();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {    //Настройка представления фрагмента. Заполненный объект
        // View возвращается активности-хосту. Первые два параметра нужны для заполнения макета. Bundle содержит данные, которые используются методом для воссоздания представления по сохраненному
        // состоянию.
        View v = inflater.inflate(R.layout.fragment_crime, container, false);   //1 - идентификатор ресурса макета. 2 - родитель представления, обычно необходимо для правильной
        // настройки виджета. 3 - нужно ли вкл заполненное представление в родителя, false значит представление будет добавлено в коде активности.

        /* Подключение виджетов*/
        mTitleField = v.findViewById(R.id.crime_title);
        mTitleField.addTextChangedListener(new TextWatcher() {  //Добавляем слушателя
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCrime.setTitle(s.toString());  //Ввод пользователя. Возвращает строку, которая потом используется для задания заголовка Crime.
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDateButton = v.findViewById(R.id.crime_date);
        mDateButton.setText(mCrime.getDate().toString());   //Отображение текущей даты
        mDateButton.setEnabled(false);

        mSolvedCheckBox = v.findViewById(R.id.crime_solved);
        mSolvedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
            }
        });
        /* Подключение виджетов*/
        return v;
    }
}
